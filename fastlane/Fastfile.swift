// This file contains the fastlane.tools configuration
// You can find the documentation at https://docs.fastlane.tools
//
// For a list of all available actions, check out
//
//     https://docs.fastlane.tools/actions
//

import Foundation

class Fastfile: LaneFile {
	func customLane() {
        
        let itcTeamId = "39941800"
        let username = "iosapp@appudvikleren.dk"
        let appIdentifier = "com.softence.BlogApp"
        let appleId = "iosapp@appudvikleren.dk"
        let devPortalTeamId = "5PN99X4SRN"
        let scheme = "BlogApp"
        let xcodeproj = "BlogApp.xcodeproj"
        
        let versionNumber = getVersionNumber(xcodeproj: xcodeproj)
        
        latestTestflightBuildNumber(
            appIdentifier: appIdentifier,
            username: username,
            version: versionNumber,
            teamId: itcTeamId
        )
        
        let fastlaneContext = laneContext()
        
        let currentBuildNumber = fastlaneContext["LATEST_TESTFLIGHT_BUILD_NUMBER"] as! Int
        
        // Incremeting build number
        let newBuildNumber: Int = currentBuildNumber + 1
        
        incrementBuildNumber(
            buildNumber: String(newBuildNumber),
            xcodeproj: xcodeproj
        )
        
        buildIosApp(scheme: scheme)
        
        uploadToTestflight(
            username: username,
            appIdentifier: appIdentifier,
            appleId: appleId,
            teamId: itcTeamId,
            devPortalTeamId: devPortalTeamId
        )
	}
}
